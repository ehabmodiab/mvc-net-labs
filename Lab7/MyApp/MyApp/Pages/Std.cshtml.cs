using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MyApp.Pages
{
  public class StdModel : PageModel
  {
    public List<Student> Students { get; set; }
    public void OnGet()
    {
      var db = new ExamdbContext();
      Students = db.Students
        .Include(s => s.Dept)
        .ToList();
    }
  }
}

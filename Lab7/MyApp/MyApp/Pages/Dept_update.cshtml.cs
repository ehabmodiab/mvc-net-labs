using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MyApp.Pages
{
  public class Dept_updateModel : PageModel
  {
    private readonly ExamdbContext _context;
    [BindProperty]
    public Department Dept { get; set; }

    public Dept_updateModel(ExamdbContext context)
    {
      _context = context;
    }

    public void OnGet(int id)
    {
      Dept = _context
        .Departments
        .Where(d => d.Dept_Id == id)
        .FirstOrDefault();
    }

    public IActionResult OnPost()
		{
      var old = _context.Departments
        .Where(d => d.Dept_Id == Dept.Dept_Id)
        .FirstOrDefault();
      _context.Departments.Remove(old);
      _context.Departments.Add(Dept);
      _context.SaveChanges();
      return RedirectToPage("Dept");
		}
  }
}

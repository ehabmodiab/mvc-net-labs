﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using WebApplication.Models;

namespace WebApplication.Controllers
{
	public class CtrController : Controller
	{
		public IActionResult Index()
		{
			return View();
		}

		public IActionResult Conc()
		{
			return View();
		}

		public int GetConvConc(int amount, string from, string to)
		{
			int val = 0;
			if (from == to)
			{
				val = amount;
			}
			else
			{
				if (from == "eg") amount /= 15;
				else if (from == "eur") amount *= 10;

				switch(to)
				{
					case "eg":
						val = amount * 15;
						break;
					case "usd":
						val = amount;
						break;
					case "eur":
						val = amount * 5;
						break;
				}
			}
			return val;
		}

		public IActionResult Std()
		{
			return View();
		}

		public IActionResult ShowStd(Student std, IFormFile f)
		{
			using (var s = new FileStream(@".\wwwroot\img\" + f.FileName, FileMode.Create))
			{
				f.CopyTo(s);
			}
			ViewBag.id = std.Id;
			ViewBag.name = std.Name;
			ViewBag.age = std.Age;
			ViewBag.img = f.FileName;
			return View();
		}

		public IActionResult deg()
		{
			Dictionary<int, int> degs = new Dictionary<int, int>();
			degs.Add(1, 0);
			degs.Add(2, 0);
			degs.Add(3, 0);
			degs.Add(4, 0);
			ViewBag.dict = degs;
			return View();
		}

		public FileResult SaveImg(string file)
		{
			return File($"/img/{file}", MimeTypes.GetMimeType($"{file}.png"),"s7.jpg");
		}

		public string ShowDeg(Degree degs)
		{
			return $"{degs.Id1}: {degs.Deg1}\n{degs.Id2}: {degs.Deg2}\n{degs.Id3}: {degs.Deg3}\n{degs.Id4}: {degs.Deg4}\n";
		}
	}
}
﻿namespace WebApplication6.Models
{
	public class Course
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public virtual ICollection<Department> Departments { get; set; }
		public virtual ICollection<Std_Crs> StudentCourses { get; set; }

		public Course()
		{
			Departments = new HashSet<Department>();
			StudentCourses = new HashSet<Std_Crs>();
		}
	}
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication6.Models
{
	public class Std_Crs
	{
		[ForeignKey("Student")]
		public int StdId { get; set; }

		[ForeignKey("Course")]
		public int CrsId { get; set; }

		public int Grade { get; set; }

		public virtual Student Student { get; set; }
		public virtual Course Course { get; set; }

		public Std_Crs()
		{
			Student = new Student();
			Course = new Course();
		}
	}
}

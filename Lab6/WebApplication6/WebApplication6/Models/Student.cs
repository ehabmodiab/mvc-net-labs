﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication6.Models
{
	public class Student
	{
		public int Id { get; set; }

		[ForeignKey("Department")]
		public int DepartmentId { get; set; }
		public string Name { get; set; }
		public int Age { get; set; }

		public virtual Department Department { get; set; }
		public virtual ICollection<Std_Crs> StudentCourses { get; set; }

		public Student()
		{
			Department = new Department();
			StudentCourses = new List<Std_Crs>();
		}
	}
}

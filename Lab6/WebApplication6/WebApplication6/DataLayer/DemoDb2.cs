﻿using Microsoft.EntityFrameworkCore;
using WebApplication6.Models;

namespace WebApplication6.DataLayer
{
	public class DemoDb2 : DbContext
	{
		public DemoDb2(DbContextOptions options) : base (options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Department>()
				.HasMany<Student>(d => d.Students)
				.WithOne(s => s.Department)
				.HasForeignKey(s => s.DepartmentId);

			modelBuilder.Entity<Department>()
				.HasMany<Course>(d => d.Courses)
				.WithMany(c => c.Departments);

			modelBuilder.Entity<Std_Crs>()
				.HasKey(s => new {s.StdId, s.CrsId});

			base.OnModelCreating(modelBuilder);
		}

		public DbSet<WebApplication6.Models.Student> Student { get; set; }
	}
}

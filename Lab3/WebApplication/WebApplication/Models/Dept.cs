﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
	public class Dept
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Location { get; set; }
		public string ImgName { get; set; }
	}
}

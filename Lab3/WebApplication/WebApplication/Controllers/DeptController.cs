﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
	public class DeptController : Controller
	{
		DeptMoc db = new DeptMoc();

		public IActionResult Index()
		{
			List<Dept> depts = db.GetAllDepts();
			return View(depts);
		}

		public IActionResult CreateDept()
		{
			return View();
		}

		public IActionResult Add(Dept dept, IFormFile img)
		{
			if (img != null)
			{
				using (var s = new FileStream(@".\wwwroot\images\" + img.FileName, FileMode.Create))
				{
					img.CopyTo(s);
				}
			}
			db.AddDept(dept);
			return RedirectToAction("Index");
		}
	}
}